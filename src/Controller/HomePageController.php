<?php

declare(strict_types=1);

namespace App\Controller;



use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomePageController extends AbstractController
{
    /**
     * @Route("/main", name="main-page", methods={"GET"})
     */
    public function main(): Response
    {
        return $this->render(view:'home-page/main.html.twig');
    }
}