<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230306203122 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE "tour_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE "tour" (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE tour_user (tour_id INT NOT NULL, user_id INT NOT NULL, PRIMARY KEY(tour_id, user_id))');
        $this->addSql('CREATE INDEX IDX_ACA3366915ED8D43 ON tour_user (tour_id)');
        $this->addSql('CREATE INDEX IDX_ACA33669A76ED395 ON tour_user (user_id)');
        $this->addSql('ALTER TABLE tour_user ADD CONSTRAINT FK_ACA3366915ED8D43 FOREIGN KEY (tour_id) REFERENCES "tour" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE tour_user ADD CONSTRAINT FK_ACA33669A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE "tour_id_seq" CASCADE');
        $this->addSql('ALTER TABLE tour_user DROP CONSTRAINT FK_ACA3366915ED8D43');
        $this->addSql('ALTER TABLE tour_user DROP CONSTRAINT FK_ACA33669A76ED395');
        $this->addSql('DROP TABLE "tour"');
        $this->addSql('DROP TABLE tour_user');
    }
}
